# Código fuente de io.hackerspace.sv

Este repositorio contiene el código fuente de la API REST
que provee acceso público a los repositorios de datos
generados por el Hackerspace San Salvador.

Para simplificar el desarrollo esta API utiliza la extensión
Connexion para el framework Flask de Python. Connexion
utiliza archivos de definición de Swagger para describir
la API. Opcionalmente utiliza Swagger-UI para auto-documentar
el uso de la API.

La versión activa del repositorio está disponible en:

[https://io.hackerspace.sv/](https://io.hackerspace.sv)

## Requerimientos de Software

Esta API tiene los siguientes requerimientos:

Desarrollo:
 - Python
 - Python PIP
 - Cloudant
 - Connexion
 - Connexion Swagger UI (Opcional)

Producción
 - uWSGI
 - nginx

### Instalación de dependencias

Si tienes instalado Python puedes instalar los requerimientos
de software con el siguiente comando de PIP

    sudo pip install cloudant connexion[swagger-ui]

## Desarrollo

Para probar el funcionamiento de la API y hacer cambios rápidos
cambia a la carpeta **api**  y ejecuta el siguiente comando:

    python run.py

Se iniciará un servidor web en la dirección [http://localhost:5000](http://localhost:5000).

La carpeta **www** contiene todos los contenidos estáticos
utilizados en el sitio web.

## Producción

Se recomienda la utilización de uWSGI + nginx.

Consulta la documentación de tu distribución linux para instalar
estas dos herramientas.

### Configuración de website para nginx

Esta es la configuración mínima recomendada para nginx:

    server {
        listen [::]:80;
        listen 80;
    
        root /ruta_a_proyecto/www; # Cambiar por la ruta al directorio www
        index index.html;
        server_name cool.server.name.biz; # Cambiar por el nombre de dominio
    
        location /api {
            include uwsgi_params;
            # Cambia por la ruta al directorio api en la siguiente línea
            uwsgi_pass unix:/ruta_a_proyecto/api/listener.socket 
        }

        location / {
            try_files $uri $uri/ /index.html
        }
    }

### Configuración de uWSGI

Esta es la configuración mínima para uWSGI:

    [uwsgi]
    chdir = /ruta_a_proyecto/api # Cambia por la ruta al directorio api
    manage-script-names = true
    socket = ./listener.socket
    chmod-socket = 664
    uid = http
    gid = http
    master = true
    plugin = python
    mount = /=run:application

### Notas sobre la configuración del sistema

uWSGI corre como un proceso independiente para capturar las solicitudes
que llegan a la app en python. Si utilizas un sistema Linux que usa
SystemD el siguiente archivo de configuración de servicio podría serte
útil:

    [Unit]
    Description=uWSGI para Hackerspace IO Open-Data Repository
    Wants=network.target
    After=network.target
    
    [Service]
    KillMode=process
    KillSignal=SIGINT
    ExecStart=/usr/bin/uwsgi --ini /ruta_a_proyecto/api/uwsgi.ini

    [Install]
    WantedBy=multi-user.target

Nombra el archivo anterior como "uwsgi-servicio.service" y copialo
en la carpeta /etc/systemd/system.

Para instalar y habilitar el servicio al arranque puedes ejecutar
los siguientes comandos:

    sudo systemctl daemon-reload
    sudo systemctl enable uwsgi-servicio
    sudo systemctl start uwsgi-servicio

## Licencia de uso

Todo el código fuente incluído en este repositorio está cubierto bajo
la licencia GNU/GPL v3.0 a menos que se especifique lo contrario en
los archivos de código fuente.

    Copyright 2019 - Mario Gómez @ Hackerspace San Salvador

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

