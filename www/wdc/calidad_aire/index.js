(function() {
    // Create the connector object
    var myConnector = tableau.makeConnector();

    // Define the schema
    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
            id: "id",
            dataType: tableau.dataTypeEnum.string
        }, {
            id: "time",
            dataType: tableau.dataTypeEnum.datetime
        }, {
            id: "accurate",
            dataType: tableau.dataTypeEnum.bool
        }, {
            id: "device_address",
            dataType: tableau.dataTypeEnum.string
        }, {
            id: "device_name",
            dataType: tableau.dataTypeEnum.string
        }, {
            id: "pm1p0",
            dataType: tableau.dataTypeEnum.float
        }, {
            id: "pm2p5",
            dataType: tableau.dataTypeEnum.float
        }, {
            id: "pm4p0",
            dataType: tableau.dataTypeEnum.float
        }, {
            id: "pm10p0",
            dataType: tableau.dataTypeEnum.float
        }];

        var tableSchema = {
            id: "calidad_aire",
            alias: "Mediciones de concentracion de partículas del Hackerspace San Salvador.",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };

    // Download the data
    myConnector.getData = function(table, doneCallback) {
        $.getJSON("/api/calidad_aire", function(resp) {
            table.appendRows(resp);
            doneCallback();
        });
    };

    tableau.registerConnector(myConnector);

    // Create event listeners for when the user submits the form
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Mediciones de Concentración de Partículas"; // This will be the data source name in Tableau
            tableau.submit(); // This sends the connector object to Tableau
        });
    });
})();
