from __future__ import print_function
from datetime import datetime
import os.path
from cloudant.view import View
from cloudant.design_document import DesignDocument
from cloudant.client import Cloudant
from cloudant.query import Query
from flask import *
from app.local_config import *
import connexion

def get_all():
    client = Cloudant(
        COUCH_USER,
        COUCH_PASS,
        url=COUCH_URL,
        use_basic_auth=True,
        timeout=0.5,
        connect=True
        )
    sensor_data = client['air_quality_sensors']
    response = list()
    measurementsByDate = DesignDocument(sensor_data, '_design/measurements_by_date')
    measurementsView = View(measurementsByDate, "by-date")

    # 1000 records is not as fast but is less than the timeout.
    with measurementsView.custom_result(page_size=10000) as result:
      for doc in result:
          item = {
              'id': doc['id'],
              'time': doc['key'],
              'device_name': doc['value']['device_name'],
              'device_address': doc['value']['device_address'],
              'accurate': doc['value']['accurate'],
              'pm1p0': doc['value']['pm1p0'],
              'pm2p5': doc['value']['pm2p5'],
              'pm4p0': doc['value']['pm4p0'],
              'pm10p0': doc['value']['pm10p0']
            }
          response.append(item)

    client.disconnect()
    return response

def get_all_csv():
    response = make_response(render_template('airquality_all.csv', items = get_all()), 200)
    response.content_type = 'text/csv'
    return response
