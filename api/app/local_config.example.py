# Rename this file to local_config.py

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = ''
SAMPLE_RANGE_NAME = ''

# Couch DB info
COUCH_USER = ''
COUCH_PASS = ''
COUCH_URL  = ''
